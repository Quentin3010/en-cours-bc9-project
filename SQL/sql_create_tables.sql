USE BC9;

CREATE TABLE Shop(
    id_item INT,
    nom_categorie VARCHAR(255),
    slot INT,
    nom_item VARCHAR(255),
    prix_unitaire_achat FLOAT,
    prix_unitaire_vente FLOAT,

    PRIMARY KEY (slot, nom_categorie)
);

CREATE TABLE Achat(
    id_achat SERIAL,
    uuid VARCHAR(255),
    pseudo VARCHAR(255),
    prix_unitaire FLOAT,
    quantite INT,
    nom_item VARCHAR(255),

    PRIMARY KEY(id_achat)
);

CREATE TABLE Vente(
    id_vente SERIAL,
    uuid VARCHAR(255),
    pseudo VARCHAR(255),
    prix_unitaire FLOAT,
    quantite INT,
    nom_item VARCHAR(255),

    PRIMARY KEY(id_vente)
);

DROP TABLE IF EXISTS Player;

CREATE TABLE Player(
	uuid VARCHAR(255),
	pseudo VARCHAR(255),
	tpsJeu int,
	lastConnection VARCHAR(255),
	
	PRIMARY KEY(uuid)
);
