import csv

with open('shop.csv', 'r') as csvfile:
    with open("sql_requests.sql", "w") as file:
        file.write("USE BC9;\nDELETE FROM Shop;\n")
        reader = csv.reader(csvfile)
        next(reader) # skip la première ligne
        for row in reader:
            #On vérifie que la ligne est pas vide (sinon c'est fini)
            if not row:
                break
            #DEBUT REQUETE
            request = "INSERT INTO Shop VALUES("
            request += row[0] + ", '" + row[1] + "', " + row[2]+ ", '"  + row[3] + "', "
            #ACHAT
            if row[4] != "-":
                request += row[4] + ", "
            else:
                request += "NULL" + ", "
            #VENTE
            if row[5] != "-":
                request += row[5] + ");\n"
            else:
                request += "NULL" + ");\n"
            #REQUETE FINI
            print(request)
            file.write(request)
