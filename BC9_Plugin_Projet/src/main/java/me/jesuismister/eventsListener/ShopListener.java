package me.jesuismister.eventsListener;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import me.jesuismister.Main;
import me.jesuismister.Other;
import me.jesuismister.inventories.Shop;

public class ShopListener implements Listener {
    
    @EventHandler
    public void onMobKill(EntityDeathEvent event) {
    	if(event.getEntity().getKiller() instanceof Player) {
    		Main.getEconomy().depositPlayer(event.getEntity().getKiller(), 1);
    	}
    }

	@EventHandler
    public void onPlayerInteract(PlayerInteractEntityEvent event) {
        if(event.getRightClicked() instanceof Villager) {
            Villager villager = (Villager) event.getRightClicked();
            if(!villager.hasAI() && villager.getCustomName()!=null){
            	String name = villager.getCustomName();
            	if(Shop.getShops().containsKey(name)) {
            		event.setCancelled(true);
            		Other.sound_action(event.getPlayer());
                    event.getPlayer().openInventory(Shop.getShops().get(name).get(0));
                    event.getPlayer().getOpenInventory().setItem(49, Shop.paper);
            	}
            }
        }
    }
	
	@EventHandler
	public void click(InventoryClickEvent event) {
		ItemStack current = event.getCurrentItem();

		if (current == null) {
			return;
		}
		
		Player player = (Player) event.getWhoClicked();
		InventoryView inv = event.getView();
		
		if(inv.getTitle()!=null && inv.getTitle().contains(ChatColor.YELLOW+"")) {
			event.setCancelled(true);
			
			if(!Main.bddOn) {
				event.getWhoClicked().sendMessage(Other.bc + ChatColor.RED + "La connexion � la BDD au d�marrage du serveur a �chou�.");
				return;
			}
			
			if(current.getType().equals(Material.GRAY_STAINED_GLASS_PANE)) return;
			
			//Basique
			if(current.getType().equals(Material.ARROW)) {
				Other.sound_action(player);
				Shop.changeShopPage(player, current.getItemMeta().getDisplayName(), inv.getTitle());
			}else if(current.getType().equals(Material.BARRIER)) {
				Other.sound_action(player);
				List<String> lore = inv.getItem(0).getItemMeta().getLore();
				player.openInventory(Shop.getShops().get(lore.get(0)).get(Integer.parseInt(lore.get(1))));
				player.getOpenInventory().setItem(49, Shop.paper);
			}else if(current.getType().equals(Material.PAPER)) {
				Other.sound_action(player);
				Shop.setPaperValues();
				player.getOpenInventory().setItem(49, Shop.paper);
			}
			
			//Achat
			else if(inv.getTitle().contains("Achat") && current.getItemMeta().hasLore()) {
				String lore = current.getItemMeta().getLore().get(0).split(" ")[2];
				double prix = Double.parseDouble(lore.substring(0, lore.length()-2));
				Shop.buyItem(player, current.getType(), prix/current.getAmount(), current.getAmount());
			}
			
			//Vente
			else if(inv.getTitle().contains("Vente") && current.getItemMeta().getDisplayName().contains(ChatColor.GOLD+"")) {
				String lore = current.getItemMeta().getLore().get(0).split(" ")[3];
				double prix = Double.parseDouble(lore.substring(0, lore.length()-2));
				Shop.sellItem(player, player.getOpenInventory().getItem(28).getType(), prix, (4*9+1)*64);
			}else if(inv.getTitle().contains("Vente") && current.getItemMeta().hasLore()) {
				String lore = current.getItemMeta().getLore().get(0).split(" ")[2];
				double prix = Double.parseDouble(lore.substring(0, lore.length()-2));
				Shop.sellItem(player, current.getType(), prix/current.getAmount(), current.getAmount());
			}
			
			//Menu shop
			else if(player.getStatistic(Statistic.KILL_ENTITY, EntityType.ENDER_DRAGON)>=1) {
				try {
					if(event.getClick().equals(ClickType.LEFT) && current.getItemMeta().hasLore()) {
						player.openInventory(Shop.getAchatItem().get(current.getType().toString()));
						Other.sound_action(player);
					}else if(event.getClick().equals(ClickType.RIGHT) && current.getItemMeta().hasLore()) {
						player.openInventory(Shop.getVenteItem().get(current.getType().toString()));
						Other.sound_action(player);
					}
				}catch(Exception e) {
					Other.sound_refuser(player);
				}
			}else {
				player.sendMessage(ChatColor.RED + "Vous devez TUER un ender dragon avant de pouvoir commercer.");
			}
		}
		return;
	}
}
