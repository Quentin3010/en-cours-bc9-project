package me.jesuismister.eventsListener;

import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.jesuismister.Main;
import me.jesuismister.Other;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

@SuppressWarnings("deprecation")
public class ChatListener implements Listener{
	private static Main main;
	
	public ChatListener(Main main2) {
		setMain(main2);
	}

	private static Main getMain() {
		return main;
	}

	private static void setMain(Main main) {
		ChatListener.main = main;
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		e.setDeathMessage(heure() + ChatColor.RED + e.getDeathMessage());
	}

	@EventHandler
	public void chatFormat(AsyncPlayerChatEvent e) {
		e.setCancelled(true);
		Bukkit.broadcastMessage(heure() + ChatColor.GRAY + e.getPlayer().getName() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.RESET + e.getMessage().toString());
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		event.setJoinMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "+" + ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + event.getPlayer().getName());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		event.setQuitMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_RED + "-" + ChatColor.DARK_GRAY + "] " + ChatColor.GRAY + event.getPlayer().getName());
	}
	
	@EventHandler
	public void commandEvent(PlayerCommandPreprocessEvent e) {
		String[] msg = e.getMessage().split(" ");
		
		if (msg.length>1 && msg[0].equals("/me")) {
			e.setCancelled(true);
		}else if(msg.length>2 && (msg[0].equals("/msg") || msg[0].equals("/tell") || msg[0].equals("/w"))) {
			e.setCancelled(true);
			if (!sendPrivateMessage(e)) {
				e.getPlayer().sendMessage(Other.bc + ChatColor.RED + "Le message n'a pas pu être envoyé.");
			}
		}
	}
	
	private boolean sendPrivateMessage(PlayerCommandPreprocessEvent e) {
		String des = e.getMessage().split(" ")[1];
		Player exp = e.getPlayer();
		
		if(des.equals(exp.getName())) return false;

		String message = "";
		
		for (int i = 2; i < e.getMessage().split(" ").length; i++) {
			message += e.getMessage().split(" ")[i] + " ";
		}
	
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (des.equalsIgnoreCase(p.getName())) {

				// Message envoye a l'expediteur
				exp.sendMessage(heure() + ChatColor.DARK_AQUA + "Envoyé à " + p.getName() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.RESET + message);

				//Message envoye au destinataire
				BaseComponent[] hoverText = new ComponentBuilder(ChatColor.LIGHT_PURPLE + "Cliquez-ici pour répondre").create();
				HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, hoverText);
				
				TextComponent msg = new TextComponent(heure() + ChatColor.DARK_PURPLE + "Reçu de " + exp.getName() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.RESET + message);
				msg.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/msg " + exp.getName() + " "));
				
				BaseComponent[] messageDest = new ComponentBuilder("").append(msg).event(hoverEvent).create();
				p.spigot().sendMessage(messageDest);

				return true;
			}
		}
		return false;
	}

	@EventHandler
	public void onPlayerAdvancement(PlayerAdvancementDoneEvent e) {
		if(e.getAdvancement()==null) return;
		
		for (World wloop : getMain().getServer().getWorlds()) {
			if (wloop.getGameRuleValue(GameRule.ANNOUNCE_ADVANCEMENTS)) {
				wloop.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
			}
		}
		advancementMessage(e);
	}
	
	private void advancementMessage(PlayerAdvancementDoneEvent e) {
		if(e.getAdvancement()==null) return;
		
		Player p = e.getPlayer();
		String[] adv = e.getAdvancement().getKey().getKey().split("/");
		
		if (adv.length>=2 && !adv[0].equals("recipes") && !adv[1].contains("root") && !adv[0].equals("collection")) {
			Bukkit.broadcastMessage(heure() + ChatColor.BLUE + p.getName() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.GREEN + affichageAdv(adv));
		}
	}

	private static String affichageAdv(String adv[]) {
	    return Other.UpperOnFirstLetter(adv[1].replace("_", " "));
	}
	
	public static String heure() {
		return ChatColor.DARK_GRAY + "[" + ChatColor.YELLOW + (new Date()).getHours() + ":" + minutesAff((new Date()).getMinutes()) + ChatColor.DARK_GRAY + "] " + ChatColor.RESET;
	}
	
	private static String minutesAff(int m) {
		if (m <= 9) {
			return "0" + m;
		} else {
			return "" + m;
		}
	}
}
