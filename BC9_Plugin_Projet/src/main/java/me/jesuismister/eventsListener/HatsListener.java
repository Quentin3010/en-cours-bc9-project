package me.jesuismister.eventsListener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import me.jesuismister.Other;
import me.jesuismister.inventories.BCPack;

public class HatsListener implements Listener {
	
	@EventHandler
	public void clickInv(InventoryClickEvent event) {
		ItemStack current = event.getCurrentItem();

		if (current == null) {
			return;
		}
		
		Player player = (Player) event.getWhoClicked();
		InventoryView inv = event.getView();
		
		if(inv.getTitle()!=null && inv.getTitle().contains(ChatColor.RED+"")) {
			event.setCancelled(true);
			if(current.getType().equals(Material.PAPER) || current.getType().equals(Material.GRAY_STAINED_GLASS_PANE)) {
				return;	
			}else if(current.getType().equals(Material.CARVED_PUMPKIN) && (player.getInventory().getHelmet()==null || !player.getInventory().getHelmet().getType().equals(Material.CARVED_PUMPKIN))) {
				player.sendMessage(ChatColor.RED + "Vous devez avoir une citrouille sur la t�te.");
				Other.sound_refuser(player);
			}else if(current.getType().equals(Material.CARVED_PUMPKIN)) {
				Other.sound_accepter(player);
				BCPack.changeTexture(player, current);
			}
		}
		return;
	}
	
	@EventHandler
	public void playerInteractEvent(PlayerInteractEvent e) {
		Action a = e.getAction();
		ItemStack it = e.getItem();
		Block b = e.getClickedBlock();

		if (it == null || b == null || a == null)
			return;

		if (it.getType() == Material.CARVED_PUMPKIN && a == Action.RIGHT_CLICK_BLOCK && b.getType() != Material.AIR && it.getItemMeta().hasLore()) {
			Other.sound_refuser(e.getPlayer());
			e.setCancelled(true);
		}
	}
}
