package me.jesuismister.eventsListener;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import me.jesuismister.Other;

public class AnvilListener implements Listener{
	
	@EventHandler
    public void onFusionPreparation(PrepareAnvilEvent e) {
		if(e.getInventory().getItem(0)==null || e.getInventory().getItem(1)==null){
			return;
		}
		
		if(e.getInventory().getItem(0).getType().equals(Material.CARVED_PUMPKIN) && e.getInventory().getItem(1).getType().equals(Material.ENCHANTED_BOOK)) {
			ItemStack oldItem = e.getInventory().getItem(0);
			ItemStack newItem = Other.createItem(oldItem.getType(), oldItem.getItemMeta().getDisplayName(), 1, oldItem.getItemMeta().getLore(), oldItem.getItemMeta().getAttributeModifiers(EquipmentSlot.HEAD));
			
			if(oldItem.getItemMeta().hasCustomModelData()) {
				ItemMeta meta = newItem.getItemMeta();
				meta.setCustomModelData(oldItem.getItemMeta().getCustomModelData());
				newItem.setItemMeta(meta);
			}
			
			EnchantmentStorageMeta meta = (EnchantmentStorageMeta) (e.getInventory().getItem(1).getItemMeta());
			Map<Enchantment, Integer> enchants = meta.getStoredEnchants();

			Map<Enchantment, Integer> newEnchants = new HashMap<Enchantment, Integer>();
			for (Enchantment enchant : enchants.keySet()) {
				if(isGoodEnchant(enchant)==true) {
					newEnchants.put(enchant, enchants.get(enchant));
				}
			}
			if(!newEnchants.isEmpty()) {
				newEnchants.putAll(oldItem.getEnchantments());
				newItem.addUnsafeEnchantments(newEnchants);
				e.setResult(newItem);
			}
		}
		
		if(e.getResult().getAmount()!=1 || e.getInventory().getItem(0).getAmount()!=1) {
			e.setResult(null);
		}
    }

	private boolean isGoodEnchant(Enchantment key) {
		if(key.equals(Enchantment.PROTECTION_ENVIRONMENTAL)) return true;
		else if(key.equals(Enchantment.PROTECTION_EXPLOSIONS)) return true;
		else if(key.equals(Enchantment.PROTECTION_FIRE)) return true;
		else if(key.equals(Enchantment.PROTECTION_PROJECTILE)) return true;
		else if(key.equals(Enchantment.BINDING_CURSE)) return true;
		else if(key.equals(Enchantment.VANISHING_CURSE)) return true;
		else if(key.equals(Enchantment.OXYGEN)) return true;
		else if(key.equals(Enchantment.THORNS)) return true;
		else if(key.equals(Enchantment.WATER_WORKER)) return true;
		else return false;
	}
	
	@EventHandler
	public void click(InventoryClickEvent event) {
		ItemStack current = event.getCurrentItem();
		if (current == null || current.getType().equals(Material.AIR) || !(event.getInventory() instanceof AnvilInventory)) {
			return;
		}
		
		if(event.getAction().equals(InventoryAction.NOTHING)) {
			Player player = (Player) event.getWhoClicked();
			if(player.getExpToLevel()>=10) {
				player.getOpenInventory().setCursor(current);
				event.getInventory().setItem(0, null);
				event.getInventory().setItem(1, null);
				event.setCurrentItem(null);
				Other.sound_enclume(player);
				player.setLevel(player.getLevel() - 10);
			}else {
				player.sendMessage(ChatColor.RED + "Il vous faut au moins 10 niveaux pour pouvoir ajouter un enchantement sur votre citrouille.");
			}
		}
		
	}
}
