package me.jesuismister.eventsListener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Slab;
import org.bukkit.block.data.type.Slab.Type;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.spigotmc.event.entity.EntityDismountEvent;

@SuppressWarnings("deprecation")
public class SittingSlabListener implements Listener{

	@EventHandler
	public void playerInteractEvent(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		ItemStack it = e.getItem();
		Block b = e.getClickedBlock();

		if (!p.isSneaking() && it == null && a == Action.RIGHT_CLICK_BLOCK && b.getType() == Material.PETRIFIED_OAK_SLAB) {
			sit(p, a, b, e);
		}
	}

	public static void sit(Player p, Action a, Block b, PlayerInteractEvent e) {
		Location l = b.getLocation();
		World w = p.getWorld();
		Chicken chicken = (Chicken) w.spawnEntity(l.add(0.5D, -0.15D, 0.5D), EntityType.CHICKEN);
		
		chicken.setPassenger(p);
		chicken.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 5000000, 0, true));
		chicken.setGravity(false);
		chicken.setInvulnerable(true);
		chicken.setPersistent(true);
		chicken.setSilent(true);
		chicken.setAI(false);
		e.setCancelled(true);
	}
	
	@EventHandler
	public void playerDismountEvent(EntityDismountEvent e) {
		if (e.getDismounted().getPassenger().getType().equals(EntityType.PLAYER)) {
			Player p = (Player) e.getDismounted().getPassenger();
			if (e.getDismounted().getType().equals(EntityType.CHICKEN)) {
				Location l = p.getLocation();
				p.leaveVehicle();
				p.teleport(l.add(0D, 0.7D, 0D));
				e.getDismounted().remove();
			}
		}
	}
	
	@EventHandler
	public void blockPlaceEvent(BlockPlaceEvent e) {
		if (e.getBlockReplacedState().getType() == Material.PETRIFIED_OAK_SLAB)
			e.setCancelled(true);
		else if (e.getBlock().getType() == Material.PETRIFIED_OAK_SLAB) {
			Slab slab = (Slab) e.getBlock().getBlockData();
			slab.setType(Type.BOTTOM);
			e.getBlock().setBlockData(slab);
		}
	}
	
}
