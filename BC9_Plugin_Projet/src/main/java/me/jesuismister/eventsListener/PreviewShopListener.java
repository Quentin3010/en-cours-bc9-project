package me.jesuismister.eventsListener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import me.jesuismister.Main;
import me.jesuismister.Other;
import me.jesuismister.inventories.PreviewShop;
import me.jesuismister.inventories.Shop;

public class PreviewShopListener implements Listener {
	@EventHandler
	public void click(InventoryClickEvent event) {
		ItemStack current = event.getCurrentItem();

		if (current == null) {
			return;
		}
		
		Player player = (Player) event.getWhoClicked();
		InventoryView inv = event.getView();
		
		if(inv.getTitle()!=null && inv.getTitle().contains(ChatColor.DARK_PURPLE+"")) {
			event.setCancelled(true);
			
			if(!Main.bddOn) {
				event.getWhoClicked().sendMessage(Other.bc + ChatColor.RED + "La connexion � la BDD au d�marrage du serveur a �chou�.");
				return;
			}
			
			if(current.getType().equals(Material.GRAY_STAINED_GLASS_PANE)) return;
			
			if(inv.getTitle().contains("Shops") && !current.getType().equals(Material.PAPER)) {
				Other.sound_action(player);
				player.openInventory(PreviewShop.getShops().get(current.getItemMeta().getDisplayName()).get(0));
				PreviewShop.mainPage.setItem(49, Shop.paper);
			}
			
			//Basique
			if(current.getType().equals(Material.ARROW)) {
				Other.sound_action(player);
				PreviewShop.changeShopPage(player, current.getItemMeta().getDisplayName(), inv.getTitle());
			}else if(current.getType().equals(Material.BARRIER)) {
				Other.sound_action(player);
				player.openInventory(PreviewShop.mainPage);
			}else if(current.getType().equals(Material.PAPER)) {
				Other.sound_action(player);
				Shop.setPaperValues();
				player.getOpenInventory().setItem(49, Shop.paper);
			}
		}
		return;
	}
}
