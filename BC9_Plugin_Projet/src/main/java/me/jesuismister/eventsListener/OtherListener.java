package me.jesuismister.eventsListener;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;

import me.jesuismister.sql.Requetes;

public class OtherListener implements Listener {
	
	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
		Requetes.updatePlayerData(event.getPlayer().getUniqueId().toString(), event.getPlayer().getName());
		Requetes.insertNewPlayer(event.getPlayer().getUniqueId().toString(), event.getPlayer().getName());
    }
	
	@EventHandler
	public void onVehicleEnter(VehicleEnterEvent event) {
	    if (event.getEntered() instanceof Villager) {
	    	Villager villager = (Villager) event.getEntered();
	        if(!villager.hasAI()) event.setCancelled(true);
	    }
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event) {
		if(!event.getEntityType().equals(EntityType.PRIMED_TNT)) {
			event.blockList().clear();
		}
	    event.setCancelled(false);
	}
}
