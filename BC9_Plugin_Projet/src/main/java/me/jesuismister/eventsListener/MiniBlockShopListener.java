package me.jesuismister.eventsListener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import me.jesuismister.Main;
import me.jesuismister.Other;
import me.jesuismister.inventories.MiniBlock;

public class MiniBlockShopListener implements Listener {
	
	@EventHandler
	public void click(InventoryClickEvent event) {
		ItemStack current = event.getCurrentItem();

		if (current == null) {
			return;
		}
		
		Player player = (Player) event.getWhoClicked();
		InventoryView inv = event.getView();
		
		if(inv.getTitle()!=null && inv.getTitle().contains(ChatColor.GREEN+"")) {
			event.setCancelled(true);
			
			if(!Main.bddOn) {
				event.getWhoClicked().sendMessage(Other.bc + ChatColor.RED + "La connexion � la BDD au d�marrage du serveur a �chou�.");
				return;
			}
			
			if(current.getType().equals(Material.PAPER) || current.getType().equals(Material.GRAY_STAINED_GLASS_PANE)) {
				return;	
			}else if(current.getType().equals(Material.ARROW)) {
				Other.sound_action(player);
				MiniBlock.changeShopPage(player, current.getItemMeta().getDisplayName(), inv.getTitle());
			}else if(player.getStatistic(Statistic.KILL_ENTITY, EntityType.ENDER_DRAGON)>=1) {
				//Achat
				if(current.getItemMeta().hasLore()) {
					String lore = current.getItemMeta().getLore().get(0).split(" ")[2];
					double prix = Double.parseDouble(lore.substring(0, lore.length()-2));
					MiniBlock.buyHead(player, current, prix, 1);
				}
			}else {
				player.sendMessage(ChatColor.RED + "Vous devez obtenir l'advancement \"Free The End\" pour faire du commerce.");
				Other.sound_refuser(player);
			}
		}
		return;
	}
}
