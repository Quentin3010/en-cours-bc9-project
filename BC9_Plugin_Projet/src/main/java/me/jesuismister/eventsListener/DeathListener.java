package me.jesuismister.eventsListener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import me.jesuismister.Main;
import net.md_5.bungee.api.ChatColor;

public class DeathListener implements Listener {
	private static final String ANSI_BLUE = "\u001B[34m";
	private static final String ANSI_RESET = "\u001B[0m";
	public static Main main;

	@EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        System.out.println(ANSI_BLUE + "Death Location : " + event.getEntity().getLocation().toString() + ANSI_RESET);
        event.getEntity().playSound(event.getEntity(), Sound.ITEM_TOTEM_USE, 1, 1);
        
        Player player = event.getEntity();
        if(!player.getInventory().isEmpty()) {
        	player.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Tu es mort. Un coffre a �t� pos� avec ton stuff en " + Math.round(player.getLocation().getX()) + " " + Math.round(player.getLocation().getY()) + " " + Math.round(player.getLocation().getZ()));
            Location chestLoc = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ());
            
            int i = 0;
            int nbChest = 0;
            Inventory chestInventory = null;
            for(ItemStack item : player.getInventory()) {
            	if(item!=null) {
            		if(i==0 || i==27) {
                		Block chestBlock = player.getWorld().getBlockAt(chestLoc);
                		chestBlock.setType(Material.CHEST);
                        Chest chest = (Chest) chestBlock.getState();
                        chest.setCustomName(ChatColor.DARK_PURPLE + "Death chest de " + player.getDisplayName());
                        chestInventory = chest.getInventory();
                        chestBlock.setMetadata("death_chest", new FixedMetadataValue(main,"true"));
                        nbChest += 1;
                		chestLoc.setX(chestLoc.getX()+nbChest);
                	}
            		chestInventory.addItem(item);
            		i++;
            	}
            }
            
            event.getDrops().clear();
        }
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event) {
		if(event.getInventory()!=null && event.getInventory().getType()!=null && event.getInventory().getType().equals(InventoryType.CHEST))
        {
			if(!event.getView().getTopInventory().isEmpty()) return;
			
			Block block = event.getInventory().getLocation().getBlock();
            Chest chest =  (Chest) block.getState();
            if(chest.hasMetadata("death_chest")) {
            	block.setType(Material.AIR);
            }
        }
	}
	
	@EventHandler
	public void onBreakEvent(BlockBreakEvent event) {
		if(event.getBlock()!=null && event.getBlock().getType()!=null && event.getBlock().getType().equals(Material.CHEST))
        {
            Block block = event.getBlock().getLocation().getBlock();
            Chest chest =  (Chest) block.getState();
            if(chest.hasMetadata("death_chest")) {
            	event.setCancelled(true);
            }
        }
	}
}
