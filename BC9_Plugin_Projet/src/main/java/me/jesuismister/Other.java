package me.jesuismister;

import java.text.DecimalFormat;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.Note.Tone;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Donkey;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Llama;
import org.bukkit.entity.Mule;
import org.bukkit.entity.Parrot;
import org.bukkit.entity.Player;
import org.bukkit.entity.SkeletonHorse;
import org.bukkit.entity.Wolf;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Multimap;

public class Other {
	public static String bc = ChatColor.YELLOW + "BC" + ChatColor.DARK_GRAY + " | " + ChatColor.RESET;

	public static String printBalance(double balance) {
		DecimalFormat decimalFormat = new DecimalFormat("#.##");
		
		if(balance>=1000000) return decimalFormat.format(balance/1000000) + "M";
		else if(balance>=1000) return decimalFormat.format(balance/1000) + "k";
		
		return balance + "";
	}
	
	public static void sound_refuser(Player player) {
		player.playNote(player.getLocation(), Instrument.BASS_GUITAR, Note.sharp(1, Tone.A));
	}
	
	public static void sound_accepter(Player player) {
		player.playNote(player.getLocation(), Instrument.PIANO, Note.sharp(1, Tone.A));
	}
	
	public static void sound_action(Player player) {
		player.playNote(player.getLocation(), Instrument.STICKS, Note.sharp(1, Tone.A));
	}
	
	public static void sound_enclume(Player player) {
		player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_USE, 1, 1);
	}
	
	public static ItemStack createItem(String mat, String name, int amount, List<String> lore) {
		return createItem(Material.valueOf(mat.toUpperCase()), name, amount, lore);
	}
	
	public static ItemStack createItem(String mat, String name, int amount, List<String> lore, boolean enchant) {
		if(!enchant) return createItem(Material.valueOf(mat.toUpperCase()), name, amount, lore);
		
		ItemStack item = createItem(Material.valueOf(mat.toUpperCase()), (ChatColor.GOLD + Other.UpperOnFirstLetter(mat.replace("_", " "))), amount, lore);
		ItemMeta itemMeta = item.getItemMeta();
		
		itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 1, true);
		itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(itemMeta);

		return item;
	}
	
	public static ItemStack createItem(Material mat, String name, int amount, List<String> lore) {
		ItemStack item = new ItemStack(mat, amount);
		ItemMeta itemMeta = item.getItemMeta();
		
		if(name!=null) itemMeta.setDisplayName(name);
		if(lore!=null) itemMeta.setLore(lore);
		item.setItemMeta(itemMeta);
		
		return item;
	}
	
	public static ItemStack createItem(Material mat, String name, int amount, List<String> lore, Multimap<Attribute, AttributeModifier> attributeModifiers) {
		ItemStack item = createItem(mat, name, amount, lore);
		ItemMeta itemMeta = item.getItemMeta();
		
		itemMeta.setAttributeModifiers(attributeModifiers);
		item.setItemMeta(itemMeta);
		
		return item;
	}
	
	public static ItemStack createItem(Material mat, String name, int amount, List<String> lore, AttributeModifier modif1, AttributeModifier modif2, AttributeModifier modif3) {
		ItemStack item = createItem(mat, null, amount, lore);
		ItemMeta itemMeta = item.getItemMeta();
		
		itemMeta.addAttributeModifier(Attribute.GENERIC_ARMOR, modif1);
		if(modif2!=null) itemMeta.addAttributeModifier(Attribute.GENERIC_ARMOR_TOUGHNESS, modif2);
		if(modif3!=null) itemMeta.addAttributeModifier(Attribute.GENERIC_KNOCKBACK_RESISTANCE, modif3);
		item.setItemMeta(itemMeta);

		return item;
	}
	
	public static Inventory newPage(String title, int numPage, int pageTotal) {
		Inventory page = Bukkit.createInventory(null, 54, title);
		ItemStack item = Other.createItem(Material.GRAY_STAINED_GLASS_PANE, " ", 1, null);
		
		for(int i = 0; i<54; i++) {
			if(i<=8 || i>=45 || (i+1)%9==0 || i%9==0) page.setItem(i, item);
		}
		
		if(numPage!=1) {
			item = Other.createItem(Material.ARROW, "Précédent", 1, null);
			page.setItem(48, item);
		}
		if(numPage<pageTotal) {
			item = Other.createItem(Material.ARROW, "Suivant", 1, null);
			page.setItem(50, item);
		}
		return page;
	}
	
	public static Inventory newPreviewPage(String string, int i, int nbPage) {
		Inventory page = newPage(string, i, nbPage);
		page.setItem(49, Other.createItem(Material.BARRIER, "Retour", 1, null));
		return page;
	}

	public static String UpperOnFirstLetter(String msg) {
		StringBuilder output = new StringBuilder();
	    boolean capitalizeNext = true;
		
		for (char c : msg.toLowerCase().toCharArray()) {
	        if (Character.isWhitespace(c)) {
	        	output.append(" ");
	            capitalizeNext = true;
	        } else if (capitalizeNext) {
	            output.append(Character.toUpperCase(c));
	            capitalizeNext = false;
	        } else {
	            output.append(c);
	        }
	    }
		
		return output.toString();
	}
	
	public static boolean estUnMosntre(Entity e) {
		if (e.getType().equals(EntityType.BLAZE))
			return true;
		if (e.getType().equals(EntityType.CAVE_SPIDER))
			return true;
		if (e.getType().equals(EntityType.CREEPER))
			return true;
		if (e.getType().equals(EntityType.DRAGON_FIREBALL))
			return true;
		if (e.getType().equals(EntityType.DROWNED))
			return true;
		if (e.getType().equals(EntityType.ELDER_GUARDIAN))
			return true;
		if (e.getType().equals(EntityType.ENDER_CRYSTAL))
			return true;
		if (e.getType().equals(EntityType.ENDER_DRAGON))
			return true;
		if (e.getType().equals(EntityType.ENDERMAN))
			return true;
		if (e.getType().equals(EntityType.ENDERMITE))
			return true;
		if (e.getType().equals(EntityType.EVOKER))
			return true;
		if (e.getType().equals(EntityType.EVOKER_FANGS))
			return true;
		if (e.getType().equals(EntityType.FIREBALL))
			return true;
		if (e.getType().equals(EntityType.FISHING_HOOK))
			return true;
		if (e.getType().equals(EntityType.GIANT))
			return true;
		if (e.getType().equals(EntityType.GUARDIAN))
			return true;
		if (e.getType().equals(EntityType.HOGLIN))
			return true;
		if (e.getType().equals(EntityType.HUSK))
			return true;
		if (e.getType().equals(EntityType.ILLUSIONER))
			return true;
		if (e.getType().equals(EntityType.IRON_GOLEM))
			return true;
		if (e.getType().equals(EntityType.MAGMA_CUBE))
			return true;
		if (e.getType().equals(EntityType.PHANTOM))
			return true;
		if (e.getType().equals(EntityType.RAVAGER))
			return true;
		if (e.getType().equals(EntityType.SHULKER))
			return true;
		if (e.getType().equals(EntityType.SHULKER_BULLET))
			return true;
		if (e.getType().equals(EntityType.SILVERFISH))
			return true;
		if (e.getType().equals(EntityType.SKELETON))
			return true;
		if (e.getType().equals(EntityType.SMALL_FIREBALL))
			return true;
		if (e.getType().equals(EntityType.SNOWBALL))
			return true;
		if (e.getType().equals(EntityType.SPIDER))
			return true;
		if (e.getType().equals(EntityType.STRAY))
			return true;
		if (e.getType().equals(EntityType.PRIMED_TNT))
			return true;
		if (e.getType().equals(EntityType.TROPICAL_FISH))
			return true;
		if (e.getType().equals(EntityType.VEX))
			return true;
		if (e.getType().equals(EntityType.WITCH))
			return true;
		if (e.getType().equals(EntityType.WITHER))
			return true;
		if (e.getType().equals(EntityType.WITHER_SKELETON))
			return true;
		if (e.getType().equals(EntityType.WITHER_SKULL))
			return true;
		if (e.getType().equals(EntityType.ZOGLIN))
			return true;
		if (e.getType().equals(EntityType.ZOMBIE))
			return true;
		if (e.getType().equals(EntityType.ZOMBIE_VILLAGER))
			return true;
		if (e.getType().equals(EntityType.ZOMBIFIED_PIGLIN))
			return true;
		return false;
	}
	
	public static boolean otherEntity(Entity e) {
		if (e.getType().equals(EntityType.PLAYER))
			return true;
		else if (e.getType().equals(EntityType.ITEM_FRAME))
			return true;
		else if (e.getType().equals(EntityType.GLOW_ITEM_FRAME))
			return true;
		else if (e.getType().equals(EntityType.DROPPED_ITEM))
			return true;
		else if (e.getType().equals(EntityType.MINECART))
			return true;
		else if (e.getType().equals(EntityType.MINECART_CHEST))
			return true;
		else if (e.getType().equals(EntityType.MINECART_FURNACE))
			return true;
		else if (e.getType().equals(EntityType.MINECART_HOPPER))
			return true;
		else if (e.getType().equals(EntityType.MINECART_TNT))
			return true;
		else if (e.getType().equals(EntityType.ARMOR_STAND))
			return true;
		else if (e.getType().equals(EntityType.BOAT))
			return true;
		else if (e.getType().equals(EntityType.PAINTING))
			return true;
		else if (e.getType().equals(EntityType.VILLAGER))
			return true;
		else if (e.getType().equals(EntityType.LEASH_HITCH))
			return true;
		else if (e.getType().equals(EntityType.IRON_GOLEM))
			return true;
		else if (e.getType().equals(EntityType.DROPPED_ITEM))
			return true;

		// TOUS LES TYPES DE PETS DU JEU
		else if (e.getType().equals(EntityType.SKELETON_HORSE)) {
			SkeletonHorse horse = (SkeletonHorse) e;
			if (horse.isTamed())
				return true;
		}
		else if (e.getType().equals(EntityType.DONKEY)) {
			Donkey horse = (Donkey) e;
			if (horse.isTamed())
				return true;
		}
		else if (e.getType().equals(EntityType.HORSE)) {
			Horse horse = (Horse) e;
			if (horse.isTamed())
				return true;
		}
		else if (e.getType().equals(EntityType.MULE)) {
			Mule horse = (Mule) e;
			if (horse.isTamed())
				return true;
		}
		else if (e.getType().equals(EntityType.LLAMA)) {
			Llama llama = (Llama) e;
			if (llama.isTamed())
				return true;
		}
		else if (e.getType().equals(EntityType.WOLF)) {
			Wolf dog = (Wolf) e;
			if (dog.isTamed())
				return true;
		}
		else if (e.getType().equals(EntityType.PARROT)) {
			Parrot parrot = (Parrot) e;
			if (parrot.isTamed())
				return true;
		}
		
		else if (e.getType().equals(EntityType.ALLAY))
			return true;
		else if (e.getType().equals(EntityType.AXOLOTL))
			return true;
		else if (e.getType().equals(EntityType.CAT))
			return true;


		return false;
	}
}
