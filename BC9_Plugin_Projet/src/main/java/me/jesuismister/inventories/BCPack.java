package me.jesuismister.inventories;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.jesuismister.Main;
import me.jesuismister.Other;

public class BCPack {
	private static List<String> hats = new ArrayList<String>();

	public BCPack(Main main) {
		hats.add("None");
		hats.addAll(main.getConfig().getStringList("texture.hats-list"));
	}
	
	public static Inventory getPlayerHatsInv(Player player) {
		Inventory inv = Other.newPage(ChatColor.RED + "Hats Menu", 1, 1);
		List<String> lore;
		int slot = 0;

		for(String name : hats) {
			lore = new ArrayList<String>();
			if(player.getInventory().getHelmet()!=null && player.getInventory().getHelmet().getItemMeta().hasLore()) lore.add(player.getInventory().getHelmet().getItemMeta().getLore().get(0));
			else lore.add(ChatColor.WHITE + "Normal");
			lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + name);
			inv.setItem(slot%28 + 10 + ((slot/7)*2)%8, Other.createItem(Material.CARVED_PUMPKIN, null, 1, lore));
			slot++;
		}
		return inv;
	}

	public static void changeTexture(Player player, ItemStack newTexture) {
		ItemStack pumpkin = player.getInventory().getHelmet();
		ItemMeta pumpkinMeta = pumpkin.getItemMeta();
		List<String> lore;
		
		if(pumpkinMeta.hasLore()) {
			lore = pumpkinMeta.getLore();
			lore.set(1, newTexture.getItemMeta().getLore().get(1));
		}
		else {
			lore = new ArrayList<String>();
			lore.add(newTexture.getItemMeta().getLore().get(0));
			lore.add(newTexture.getItemMeta().getLore().get(1));
		}
		pumpkinMeta.setLore(lore);
		pumpkin.setItemMeta(pumpkinMeta);
	}
	
	public List<String> getHats() {
		return hats;
	}

	public void setHats(List<String> hats) {
		BCPack.hats = hats;
	}
}
