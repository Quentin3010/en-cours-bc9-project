package me.jesuismister.inventories;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.profile.PlayerProfile;

import me.jesuismister.Main;
import me.jesuismister.Other;
import me.jesuismister.sql.Requetes;

public class MiniBlock {
	private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
	
	private static List<Inventory> miniblocks = new ArrayList<Inventory>();

	public static void createMiniBlockInv(Main main) {
		int slot = 0;
		List<String> list = new ArrayList<String>();
		
		if (main.getConfig().isConfigurationSection("shop")) {
			for(String headCat : main.getConfig().getConfigurationSection("shop").getKeys(false)) {
				list.addAll(main.getConfig().getStringList("shop." + headCat));
			}
			for (String headUUID : list) {
				if(slot%28==0)
					miniblocks.add(Other.newPage(ChatColor.GREEN + "MiniBlock - Page " + ((slot/28)+1), Math.round(slot/28)+1, (list.size()/28)+1));
				miniblocks.get(slot/28).setItem(slot%28 + 10 + ((slot/7)*2)%8, creationDeMiniBloc(headUUID));
				slot++;
			}
		}else {
			miniblocks.add(Other.newPage(ChatColor.GREEN + "MiniBlock - Page 1", 0, 0));
		}
	}
	
	public static ItemStack getCustomSkull(String base64Texture) {		
		if (base64Texture.isEmpty())
			return new ItemStack(Material.PLAYER_HEAD);
		
		ItemStack head = new ItemStack(Material.PLAYER_HEAD);

		try {
			PlayerProfile profile = Bukkit.getServer().createPlayerProfile(UUID.randomUUID(), "");
			profile.getTextures().setSkin(new URL(getURLFromBase64(base64Texture)));
			SkullMeta meta = (SkullMeta) head.getItemMeta();
			meta.setOwnerProfile(profile);
			head.setItemMeta(meta);
			
			List<String> lore = new ArrayList<String>();
			ItemMeta itemMeta = head.getItemMeta();
			lore.add(ChatColor.RED + "Achat : 1000" + ChatColor.YELLOW + " �");
			itemMeta.setLore(lore);
			head.setItemMeta(itemMeta);
			
		} catch (MalformedURLException e) {
			System.out.println(ANSI_RED + "Erreur lors de la cr�ation d'une t�te custom" + ANSI_RESET);
			e.printStackTrace();
		}
		return head;
	}

	private static String getURLFromBase64(String base64) {
		return new String(Base64.getDecoder().decode(base64.getBytes())).replace("{\"textures\":{\"SKIN\":{\"url\":\"", "").replace("\"}}}", "");
    }
	
	@SuppressWarnings("deprecation")
	public static ItemStack creationDeMiniBloc(String id) {
		if (id.length() <= 16) {
			ItemStack head = new ItemStack(Material.PLAYER_HEAD, 1);
			SkullMeta headSM = (SkullMeta) head.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.RED + "Achat : 1000" + ChatColor.YELLOW + " �");
			headSM.setLore(lore);
			headSM.setOwner(id);
			head.setItemMeta(headSM);
			return head;
		}else {
			return getCustomSkull(id);
		}
	}

	public static void buyHead(Player player, ItemStack head, double buyingPrice, int quantite) {
		if(!Main.bddOn) {
			player.sendMessage(Other.bc + ChatColor.RED + "La connexion � la BDD au d�marrage du serveur a �chou�.");
			return;
		}
		
		if((Main.getEconomy().getBalance(player)-(quantite*buyingPrice))<0) {
			Other.sound_refuser(player);
			return;
		}
		
		boolean slotLibre = false;
		ItemStack[] inv = player.getInventory().getContents();
		int i = 0;
		
		while(!slotLibre && i<36) {
			if(inv[i]==null || (inv[i].equals(head) && inv[i].getAmount()+quantite<=64)) slotLibre = true;
			i++;
		}
		
		if(slotLibre==true) {
			ItemStack headBuy = head.clone();
			ItemMeta headMeta = headBuy.getItemMeta();
			headMeta.setLore(null);
			headBuy.setItemMeta(headMeta);
			
			player.getInventory().addItem(headBuy);
			Main.getEconomy().withdrawPlayer(player, quantite*buyingPrice);
			Requetes.insertNewAchat(player.getUniqueId().toString(), player.getName(), buyingPrice, quantite, head.getType().toString());
			Other.sound_accepter(player);
		}else {
			Other.sound_refuser(player);
		}
	}
	
	public static void changeShopPage(Player p, String arrow, String invTitle) {
		int nbPage = Integer.parseInt(invTitle.split(" ")[3]);
		String shopName = invTitle.split(" ")[0];
		
		shopName = shopName.substring(2, shopName.length());
		try {
			if(arrow.equals("Suivant")) {
				p.openInventory(miniblocks.get(nbPage));
			}else if(arrow.equals("Pr�c�dent")) {
				p.openInventory(miniblocks.get(nbPage-2));
			}
		}catch(Exception e) {
			System.out.println(ANSI_RED + "Erreur lors d'une tentative de changer de page ("+invTitle+")" + ANSI_RESET);
			e.printStackTrace();
		}
	}
	
	public static List<Inventory> getMiniblocks() {
		return miniblocks;
	}

	public static void setMiniblocks(List<Inventory> miniblocks) {
		MiniBlock.miniblocks = miniblocks;
	}
	
}
