package me.jesuismister.inventories;

import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import me.jesuismister.Other;

public class PreviewShop {
	private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
	
	public static Map<String, List<Inventory>> shops;
	public static Inventory mainPage;
	
	public static void initPreviewShop() {
		mainPage = Other.newPage(ChatColor.DARK_PURPLE + "Shops", 1, 1);
		mainPage.setItem(20, Other.createItem(Material.CARROT, "Agriculteur", 1, null));
		mainPage.setItem(21, Other.createItem(Material.SEA_LANTERN, "Aquatique", 1, null));
		mainPage.setItem(22, Other.createItem(Material.GRASS_BLOCK, "Basique", 1, null));
		mainPage.setItem(23, Other.createItem(Material.END_STONE, "End", 1, null));
		mainPage.setItem(24, Other.createItem(Material.SUNFLOWER, "Fleuriste", 1, null));
		
		mainPage.setItem(29, Other.createItem(Material.OAK_LOG, "Forestier", 1, null));
		mainPage.setItem(30, Other.createItem(Material.CRIMSON_NYLIUM, "Nether", 1, null));
		mainPage.setItem(32, Other.createItem(Material.REDSTONE, "Redstone", 1, null));
		mainPage.setItem(33, Other.createItem(Material.IRON_INGOT, "Ressources", 1, null));
		
		mainPage.setItem(49, Shop.paper);
	}
	
	public static void changeShopPage(Player p, String arrow, String invTitle) {
		String shopName = invTitle.split(" ")[0];
		shopName = shopName.substring(2, shopName.length());
		int nbPage = Integer.parseInt(invTitle.split(" ")[3]);
		
		try {
			if(arrow.equals("Suivant")) {
				p.openInventory(shops.get(shopName).get(nbPage));
			}else if(arrow.equals("Précédent")) {
				p.openInventory(shops.get(shopName).get(nbPage-2));
			}
		}catch(Exception e) {
			System.out.println(ANSI_RED + "Erreur lors d'une tentative de changer de page ("+invTitle+")" + ANSI_RESET);
			e.printStackTrace();
		}
	}
	
	public static Map<String, List<Inventory>> getShops() {
		return shops;
	}

	public static void setShops(Map<String, List<Inventory>> shops) {
		PreviewShop.shops = shops;
	}
}
