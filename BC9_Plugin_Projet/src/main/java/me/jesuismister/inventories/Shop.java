package me.jesuismister.inventories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.jesuismister.Main;
import me.jesuismister.Other;
import me.jesuismister.sql.Article;
import me.jesuismister.sql.DS;
import me.jesuismister.sql.Requetes;

public class Shop {
	private static Map<String, List<Inventory>> shops;
	private static Map<String, Inventory> achatItem;
	private static Map<String, Inventory> venteItem;
	
	private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    
    private static String articleDuJour;
    private static List<String> articleTropVendu = new ArrayList<String>();
    public static ItemStack paper;
	
	public static void createMapShops() {
		articleDuJour = Requetes.getArticleDuJour().toUpperCase();
		setPaperValues();
		
		Map<String, List<Inventory>> shops = new HashMap<String, List<Inventory>>();
		Map<String, List<Inventory>> previewShops = new HashMap<String, List<Inventory>>();
		setAchatItem(new HashMap<String, Inventory>());
		setVenteItem(new HashMap<String, Inventory>());
		
		try (Connection con = DS.getConnection()) {
			String query = "SELECT nom_categorie, round((count(*)-1)/28 + 0.5) as nbPage FROM `Shop` group by nom_categorie;";
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			
			String nomCat;
			int nbPage;
			
			while(rs.next()) {
				List<Inventory> pages = new ArrayList<Inventory>();
				List<Inventory> previewPages = new ArrayList<Inventory>();
				nbPage = rs.getInt("nbPage");
				nomCat = rs.getString("nom_categorie");
				int i = 0;
				while(i<nbPage) {
					pages.add(Other.newPage((ChatColor.YELLOW + nomCat + " - Page " + (i+1)), i+1, nbPage));
					previewPages.add(Other.newPreviewPage((ChatColor.DARK_PURPLE + nomCat + " - Page " + (i+1)), i+1, nbPage));
					i++;
				}
				shops.put(nomCat, pages);
				previewShops.put(nomCat, previewPages);
			}
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "Erreur lors de l'init du shop " + ANSI_RESET);
			e.printStackTrace();
		}
		setShops(shops);	
		PreviewShop.setShops(previewShops);
	}

	public static void setItemFromBDD() {
		Map<String, List<Inventory>> shops = getShops();
		
		try (Connection con = DS.getConnection()) {
			String query = "select * from Shop order by nom_categorie, slot;";
			PreparedStatement ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			
			String nom_categorie;
			int slot;
			String nom_item;
			boolean enchant;
			int coeff;
			float buying_price;
			float selling_price;
			
			while(rs.next()) {
				nom_categorie = rs.getString("nom_categorie");
				slot = rs.getInt("slot");
				nom_item = rs.getString("nom_item").toUpperCase();
				
				enchant = false;
				coeff = 1;
				if(nom_item.equals(articleDuJour)) {
					enchant = true;
					coeff = 2;
				}
				
				buying_price = rs.getFloat("prix_unitaire_achat")/coeff;
				selling_price = rs.getFloat("prix_unitaire_vente")*coeff;
				
				try{
					List<String> lore = new ArrayList<String>();
					if(buying_price!=0) {
						achatItem.put(nom_item, createAchatVentePage("Achat", nom_categorie, slot/28, nom_item, buying_price));
						lore.add(ChatColor.RED + "Achat : " + buying_price + ChatColor.YELLOW + " �");
					}else {
						lore.add(ChatColor.RED + "Achat : --.-" + ChatColor.YELLOW + " �");
					}
					
					if(selling_price!=0) {
						venteItem.put(nom_item, createAchatVentePage("Vente", nom_categorie, slot/28, nom_item, selling_price));
						lore.add(ChatColor.GREEN + "Vente : " + selling_price + ChatColor.YELLOW + " �");
					}else {
						lore.add(ChatColor.GREEN + "Vente : --.-" + ChatColor.YELLOW + " �");
					}
					
					shops.get(nom_categorie).get(slot/28).setItem(slot%28 + 10 + ((slot/7)*2)%8, Other.createItem(nom_item, null, 1, lore, enchant));
					PreviewShop.shops.get(nom_categorie).get(slot/28).setItem(slot%28 + 10 + ((slot/7)*2)%8, Other.createItem(nom_item, null, 1, lore, enchant));
				}catch(Exception e) {
					System.out.println(ANSI_RED + "Erreur lors de l'ajout de l'items = " + nom_item + ", dans le shop" + ANSI_RESET);
					e.printStackTrace();
					break;
				}
			}
			
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "Erreur lors de l'ajout des items dans la base " + ANSI_RESET);
			e.printStackTrace();
		}
		setShops(shops);
	}
	
	private static Inventory createAchatVentePage(String type, String nom_cat, int num_page, String nom_item, double prix) {
		Inventory inv = Other.newPage(ChatColor.YELLOW + type + " : " + Other.UpperOnFirstLetter(nom_item.replace("_", " ")), 1, 1);
		
		try {
			List<String> lore = new ArrayList<String>();
			
			if(type.equals("Achat")) {
				inv.setItem(13, Other.createItem(Material.RED_CONCRETE, ChatColor.RED + "Section Achat", 1, null));
			}else {
				inv.setItem(13, Other.createItem(Material.LIME_CONCRETE, ChatColor.GREEN + "Section Vente", 1, null));

				lore.add(ChatColor.GREEN + "Prix Unitaire : " + prix + ChatColor.YELLOW + " �");
				inv.setItem(40, Other.createItem(Material.CHEST, ChatColor.GOLD + "Tout vendre !!", 1, lore));
				lore.clear();
			}
			
			int quantite;
			
			lore.add(""+nom_cat);
			lore.add(""+num_page);
			inv.setItem(0, Other.createItem(Material.GRAY_STAINED_GLASS_PANE, " ", 1, lore));
			
			for(int i = 28; i<=34; i++) {
				quantite = ((int) Math.pow(2, i-28));
				lore = new ArrayList<String>();
				if(type.equals("Achat")) {
					lore.add(ChatColor.RED + "Prix : " + quantite*prix + ChatColor.YELLOW + " �");
				}else {
					lore.add(ChatColor.GREEN + "Prix : " + quantite*prix + ChatColor.YELLOW + " �");
				}
				inv.setItem(i, Other.createItem(nom_item, null, quantite, lore));
			}
			inv.setItem(49, Other.createItem(Material.BARRIER, ChatColor.RED + "Retour", 1, null));
		}catch(Exception e) {
			System.out.println(ANSI_RED + "Erreur lors de la cr�ation du shop de " + nom_item + " de " + type + ANSI_RESET);
			e.printStackTrace();
		}
		return inv;
	}
	
	public static void sellItem(Player player, Material mat, double sellingPrice, int quantite) {
		if(Article.isTop5(Requetes.getTOP5Ventes(), mat)) {
			player.sendMessage(ChatColor.RED + "L'article fait actuellement parti du TOP5 des ventes.");
			Other.sound_refuser(player);
			return;
		}
		
		if(!Main.bddOn) {
			player.sendMessage(Other.bc + ChatColor.RED + "La connexion � la BDD au d�marrage du serveur a �chou�.");
			return;
		}
		
		if(player.getInventory().contains(mat)) {
			int quantiteToRemove = quantite;
			int amount;
			
			for(int i = 0; i<36; i++) {
				if(player.getInventory().getItem(i)!=null && player.getInventory().getItem(i).getType().equals(mat)) {
					amount = player.getInventory().getItem(i).getAmount();
					if(amount>quantiteToRemove) {
						ItemStack itemInv = player.getInventory().getItem(i);
						itemInv.setAmount(amount-quantiteToRemove);
						player.getInventory().setItem(i, itemInv);
						quantiteToRemove = 0;
						break;
					}else if(amount==quantiteToRemove){
						player.getInventory().setItem(i, null);
						quantiteToRemove -= amount;
						break;
					}else {
						player.getInventory().setItem(i, null);
						quantiteToRemove -= amount;
					}
				}
			}
			Main.getEconomy().depositPlayer(player, (quantite-quantiteToRemove)*sellingPrice);
			Requetes.insertNewVente(player.getUniqueId().toString(), player.getName(), sellingPrice, (quantite-quantiteToRemove), mat.toString());
			Shop.setPaperValues();
			Other.sound_accepter(player);
		}else {
			Other.sound_refuser(player);
		}
	}

	public static void buyItem(Player player, Material mat, double buyingPrice, int quantite) {
		if(!Main.bddOn) {
			player.sendMessage(Other.bc + ChatColor.RED + "La connexion � la BDD au d�marrage du serveur a �chou�.");
			return;
		}
		
		if((Main.getEconomy().getBalance(player)-(quantite*buyingPrice))<0) {
			Other.sound_refuser(player);
			return;
		}
		
		boolean slotLibre = false;
		ItemStack[] inv = player.getInventory().getContents();
		int i = 0;
		
		while(!slotLibre && i<36) {
			if(inv[i]==null || (inv[i].getType().equals(mat) && inv[i].getAmount()+quantite<=64)) slotLibre = true;
			i++;
		}
		
		if(slotLibre==true) {
			player.getInventory().addItem(new ItemStack(mat, quantite));
			Main.getEconomy().withdrawPlayer(player, quantite*buyingPrice);
			Requetes.insertNewAchat(player.getUniqueId().toString(), player.getName(), buyingPrice, quantite, mat.toString());
			Other.sound_accepter(player);
		}else {
			Other.sound_refuser(player);
		}
	}
	
	public static void changeShopPage(Player p, String arrow, String invTitle) {
		String shopName = invTitle.split(" ")[0];
		shopName = shopName.substring(2, shopName.length());
		int nbPage = Integer.parseInt(invTitle.split(" ")[3]);
		
		try {
			if(arrow.equals("Suivant")) {
				p.openInventory(shops.get(shopName).get(nbPage));
			}else if(arrow.equals("Pr�c�dent")) {
				p.openInventory(shops.get(shopName).get(nbPage-2));
			}
		}catch(Exception e) {
			System.out.println(ANSI_RED + "Erreur lors d'une tentative de changer de page ("+invTitle+")" + ANSI_RESET);
			e.printStackTrace();
		}
	}
	
	public static void setPaperValues() {
		List<String> lore = new ArrayList<String>();
		
		lore.add(ChatColor.LIGHT_PURPLE+ "Article du jour :");
		lore.add(ChatColor.WHITE + "- " + articleDuJour);
		lore.add("");
		lore.add(ChatColor.LIGHT_PURPLE + "TOP 5 des ventes :");
		
		int i = 1;
		
		for(Article article : Requetes.getTOP5Ventes()) {
			lore.add(article.toTxt(i));
			i++;
		}
		
		paper = Other.createItem(Material.PAPER, ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "NEWS DU JOUR", 1, lore);
	}
	
	public static Map<String, List<Inventory>> getShops() {
		return shops;
	}

	private static void setShops(Map<String, List<Inventory>> shops) {
		Shop.shops = shops;
	}

	public static Map<String, Inventory> getAchatItem() {
		return achatItem;
	}

	public static void setAchatItem(Map<String, Inventory> achatItem) {
		Shop.achatItem = achatItem;
	}

	public static Map<String, Inventory> getVenteItem() {
		return venteItem;
	}

	public static void setVenteItem(Map<String, Inventory> venteItem) {
		Shop.venteItem = venteItem;
	}

	public static List<String> getArticleTropVendu() {
		return articleTropVendu;
	}

	public static void setArticleTropVendu(List<String> articleTropVendu) {
		Shop.articleTropVendu = articleTropVendu;
	}
}
