package me.jesuismister.tabMenu;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.jesuismister.Main;
import me.jesuismister.sql.Requetes;
import net.milkbowl.vault.economy.Economy;

public class TablistTask extends BukkitRunnable {
	private Main main;
	private static Economy economy = null;
	
	private static List<Double> listTps = new ArrayList<Double>();

	@SuppressWarnings("static-access")
	public TablistTask(Main main, Economy economy) {
		this.main = main;
		this.economy = economy;
	}

	public void run() {
		String tps = tps();
		for (Player p : Bukkit.getOnlinePlayers()) {
			main.getTablistManager().setTablist(p, Requetes.getTpsJeu(p.getUniqueId()), tps, economy.getBalance(p.getPlayer()));
		}
	}
	
	public static String tps() {
		double tps = getTpsMoyen();
		
		if (tps >= 20)
			return ChatColor.GREEN + "" + "20.0";
		if (tps >= 19)
			return ChatColor.GREEN + "" + tps;
		else if (tps >= 15)
			return ChatColor.YELLOW + "" + tps;
		else if (tps >= 12)
			return ChatColor.GOLD + "" + tps;
		else if (tps >= 8)
			return ChatColor.RED + "" + tps;
		else
			return ChatColor.DARK_RED + "" + tps;
	}
	
	private static double getTpsMoyen() {
		double tpsActuelle = Lag.getTPS();
		double tpsMoyen = 0;
		
		if(listTps.size()>=10) {
			listTps.remove(0);
		}
		listTps.add(tpsActuelle);
		
		for(Double tps : listTps) {
			tpsMoyen += tps;
		}
		
		return (double) ((int) (tpsMoyen/listTps.size() * 10.0) / 10.0);
	}

}
