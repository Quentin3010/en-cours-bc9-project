package me.jesuismister.tabMenu;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.jesuismister.Other;

public class TablistManager {
	public void setTablist(Player p, int heuresJeu, String tps, double balance) {
		String Header = ChatColor.RED + ">" + ChatColor.YELLOW + ">" + ChatColor.GREEN + ">" + ChatColor.AQUA + " Best Cube " + ChatColor.GREEN + "<" + ChatColor.YELLOW + "<" + ChatColor.RED + "<";
		String tabHeure = ChatColor.GRAY + "Temps de jeu : " + ChatColor.YELLOW + heuresJeu + "h";
		String tabMoney = ChatColor.GRAY + "Money : " + ChatColor.YELLOW + Other.printBalance(balance) + " �" + ChatColor.RESET;
		String tabPing = ChatColor.GRAY + "Ping: " + ping(p);
		String tabTPS = ChatColor.GRAY + "TPS: " + tps;
		
		p.setPlayerListHeaderFooter("  " + Header + "  \n", "\n  " + tabHeure + "  \n  " + tabMoney + "  \n\n  " + tabPing + ChatColor.DARK_GRAY + " | " + tabTPS + "  ");
	}
	
	private String ping(Player p) {
		int ping = p.getPing();
		
		if (ping < 100)
			return ChatColor.GREEN + "" + ping + "ms";
		else if (ping < 200)
			return ChatColor.YELLOW + "" + ping + "ms";
		else if (ping < 500)
			return ChatColor.RED + "" + ping + "ms";
		else
			return ChatColor.DARK_RED + "" + ping + "ms";
	}
}
