package me.jesuismister.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import me.jesuismister.inventories.Shop;

public class Requetes {
	private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
	
    public static void insertNewAchat(String uuid, String pseudo, double buyingPrice, int quantite, String nom_item) {
		try (Connection con = DS.getConnection()) {
			String query = "INSERT INTO Achat VALUES(DEFAULT, ?, ?, ?, ?, ?);";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, uuid);
			ps.setString(2, pseudo);
			ps.setDouble(3, buyingPrice);
			ps.setInt(4, quantite);
			ps.setString(5, nom_item);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Ajout de l'achat impossible" + ANSI_RESET);
			e.printStackTrace();
		}
	}
    
    public static void insertNewVente(String uuid, String pseudo, double sellingPrice, int quantite, String nom_item) {
		try (Connection con = DS.getConnection()) {
			String query = "INSERT INTO Vente VALUES(DEFAULT, ?, ?, ?, ?, ?);";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, uuid);
			ps.setString(2, pseudo);
			ps.setDouble(3, sellingPrice);
			ps.setInt(4, quantite);
			ps.setString(5, nom_item);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Ajout de la vente impossible" + ANSI_RESET);
			e.printStackTrace();
		}
	}

	public static String getArticleDuJour() {
		try (Connection con = DS.getConnection()) {
			String query = "SELECT nom_item FROM Shop WHERE prix_unitaire_vente IS NOT NULL ORDER BY RAND() LIMIT 1;";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			String articleDuJour = rs.getString("nom_item");
			con.close();
			return articleDuJour.toUpperCase();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Obtention de l'article du jour impossible" + ANSI_RESET);
			e.printStackTrace();
		}
		return "ERROR";
	}
	
	public static List<Article> getTOP5Ventes(){
		List<Article> list = new ArrayList<Article>();
		try (Connection con = DS.getConnection()) {
			String query = "SELECT nom_item, SUM(quantite) AS quantite_vendue, (SUM(quantite) / (SELECT SUM(quantite) FROM Vente)) * 100 AS pourcentage FROM Vente GROUP BY nom_item HAVING SUM(quantite)>=1000 ORDER BY quantite_vendue DESC, nom_item ASC LIMIT 5;";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Article article = new Article(rs.getString("nom_item"), rs.getInt("quantite_vendue"), rs.getDouble("pourcentage"));
				list.add(article);
				if(article.getProportionDesVentes()>=20) Shop.getArticleTropVendu().add(rs.getString("nom_item").toUpperCase());
			}
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Obtention du TOP5 des ventes impossible" + ANSI_RESET);
			e.printStackTrace();
		}
		return list;
	}
	
	public static void insertNewPlayer(String uuid, String pseudo) {
		try (Connection con = DS.getConnection()) {
			String query = "INSERT INTO Player(uuid, pseudo, tpsJeu, lastConnection) SELECT ?, ?, ?, ? WHERE NOT EXISTS (SELECT * FROM Player WHERE uuid = ?);";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, uuid);
			ps.setString(2, pseudo);
			ps.setInt(3, 0);
			ps.setString(4, (new Date()).toString());
			ps.setString(5, uuid);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Ajout du joueur dans la base impossible" + ANSI_RESET);
			e.printStackTrace();
		}
	}
	
	public static void updatePlayerData(String uuid, String pseudo) {
		try (Connection con = DS.getConnection()) {
			String query = "UPDATE Player SET pseudo = ?, lastConnection = ? WHERE uuid = ?;";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, pseudo);
			ps.setString(2, (new Date()).toString());
			ps.setString(3, uuid);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Update du joueur dans la base impossible" + ANSI_RESET);
			e.printStackTrace();
		}
	}
	
	public static void updateTimePlay(String request) {
		try (Connection con = DS.getConnection()) {
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(request);
			ps.executeUpdate();
			con.close();
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Update du temps de jeu des joueurs" + ANSI_RESET);
			e.printStackTrace();
		}
	}
	
	public static int getTpsJeu(UUID uuid){
		try (Connection con = DS.getConnection()) {
			String query = "SELECT tpsJeu FROM Player WHERE uuid = ?;";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			ps.setString(1, uuid.toString());
			ResultSet rs = ps.executeQuery();
			rs.next();
			int res = rs.getInt("tpsJeu")/3600;
			con.close();
			return res;
		} catch (Exception e) {
			System.out.println(ANSI_RED + "ERREUR : Get tps du jeu de " + uuid.toString() + ANSI_RESET);
			e.printStackTrace();
		}
		return 0;
	}
}
