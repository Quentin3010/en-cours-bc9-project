package me.jesuismister.crafts;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.SmithingRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import me.jesuismister.Main;
import me.jesuismister.Other;



public class CraftsCustoms {

	public CraftsCustoms(Main main) {
		pumpkinCraft(main, "Leather", ChatColor.GOLD, 1, 1.0, 0.0, 0.0);
		pumpkinCraft(main, "Iron_ingot", ChatColor.WHITE, 2, 2.0, 0.0, 0.0);
		pumpkinCraft(main, "Gold_ingot", ChatColor.YELLOW, 3, 2.0, 0.0, 0.0);
		pumpkinCraft(main, "Diamond", ChatColor.AQUA, 4, 3.0, 2.0, 0.0);
		netheritePumpkin(main, "Netherite_ingot", ChatColor.DARK_GRAY, 5, 3.0, 3.0, 0.1);
	}
	
	public void pumpkinCraft(Main main, String mat, ChatColor color, int customModelData, double value1, double value2, double value3) {
		ItemStack pumpkin = createPumpkin(mat, color, customModelData, value1, value2, value3);
		ShapedRecipe craft = new ShapedRecipe(NamespacedKey.minecraft(("pumpkin" + mat).toLowerCase()), pumpkin);
		
		craft.shape("%%%","%*%");
		craft.setIngredient('*', Material.CARVED_PUMPKIN);
		craft.setIngredient('%', Material.valueOf(mat.toUpperCase()));
		
		main.getServer().addRecipe(craft);
	}
	
	public void netheritePumpkin(Main main,String mat, ChatColor color, int customModelData, double value1, double value2, double value3) {
		ItemStack netherite_pumpkin = createPumpkin(mat, color, customModelData, value1, value2, value3);
		SmithingRecipe recipe = new SmithingRecipe(new NamespacedKey(main, "pumpkin"+mat), netherite_pumpkin, new RecipeChoice.ExactChoice(new ItemStack(Material.CARVED_PUMPKIN)), new RecipeChoice.MaterialChoice(Material.NETHERITE_INGOT));

		main.getServer().addRecipe(recipe);
	}
	
	public ItemStack createPumpkin(String mat, ChatColor color, int customModelData, double value1, double value2, double value3) {
		List<String> lore = new ArrayList<String>();
		lore.add(color+mat.split("_")[0]);
		lore.add(ChatColor.GRAY + "" + ChatColor.ITALIC + "None");
		
		AttributeModifier modif1 = new AttributeModifier(UUID.randomUUID(), "GENERIC_ARMOR", value1, Operation.ADD_NUMBER, EquipmentSlot.HEAD);
		AttributeModifier modif2 = value2==0 ? null : new AttributeModifier(UUID.randomUUID(), "GENERIC_ARMOR_TOUGHNESS", value2, Operation.ADD_NUMBER, EquipmentSlot.HEAD); 
		AttributeModifier modif3 = value2==0 ? null : new AttributeModifier(UUID.randomUUID(), "GENERIC_KNOCKBACK_RESISTANCE", value3, Operation.ADD_NUMBER, EquipmentSlot.HEAD);
		
		ItemStack pumpkin = Other.createItem(Material.CARVED_PUMPKIN, null, 1, lore, modif1, modif2, modif3);
		ItemMeta meta = pumpkin.getItemMeta();
		
		meta.setCustomModelData(customModelData);
		pumpkin.setItemMeta(meta);
		
		return pumpkin;
	}
}
