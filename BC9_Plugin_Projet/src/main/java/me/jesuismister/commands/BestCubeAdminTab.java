package me.jesuismister.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class BestCubeAdminTab implements TabCompleter {
	List<String> arguments = new ArrayList<String>();

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String msg, String[] args) {
		if (arguments.isEmpty()) {
			arguments.add("where");
			arguments.add("broadcast");
			arguments.add("purge");
			arguments.add("money");
			arguments.add("help");
		}

		List<String> result = new ArrayList<String>();
		if (args.length == 1) {
			for (String s : arguments) {
				if (s.toLowerCase().startsWith(args[0].toLowerCase())) {
					result.add(s);
				}
			}
			return result;
		}

		return null;
	}

}
