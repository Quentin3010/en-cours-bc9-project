package me.jesuismister.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.jesuismister.Main;
import me.jesuismister.Other;
import me.jesuismister.eventsListener.ChatListener;
import me.jesuismister.inventories.BCPack;
import me.jesuismister.inventories.MiniBlock;
import me.jesuismister.inventories.PreviewShop;
import me.jesuismister.inventories.Shop;
import net.md_5.bungee.api.ChatColor;

public class CommandsBC implements CommandExecutor {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {

		if (sender instanceof Player && args.length >= 1) {
			Player player = (Player) sender;
			if (args[0].equalsIgnoreCase("miniblocks")) {
				Other.sound_action(player);
				player.openInventory(MiniBlock.getMiniblocks().get(0));
				return true;
			}else if (args.length==1 && args[0].equalsIgnoreCase("pack")) {
				Other.sound_action(player);
				player.openInventory(BCPack.getPlayerHatsInv(player));
				return true;
			}else if (args.length==1 && args[0].equalsIgnoreCase("book")) {
				Other.sound_accepter(player);
				book(player);
				return true;
			}else if (args.length==1 && args[0].equalsIgnoreCase("coords")) {
				Other.sound_action(player);
				coords(player);
				return true;
			}else if (args[0].equalsIgnoreCase("pay")) {
				pay(player, args);
				return true;
			}else if (args.length==1 && args[0].equalsIgnoreCase("help")) {
				Other.sound_action(player);
				help(player);
				return true;
			}else if (args.length==1 && args[0].equalsIgnoreCase("shop")) {
				Other.sound_action(player);
				shop(player);
				return true;
			}else {
				Other.sound_refuser(player);
				sender.sendMessage(Other.bc + ChatColor.RED + "La commande n'est pas reconnu.");
			}
		}
		return false;
	}
	
	private void shop(Player player) {
		PreviewShop.mainPage.setItem(49, Shop.paper);
		player.openInventory(PreviewShop.mainPage);
	}

	private void help(Player player) {
		player.sendMessage(Other.bc +  ChatColor.BLUE + "Voici la liste des commandes du plugin Best Cube :");
		player.sendMessage(singleLineHelp("book", "Permet d'obtenir le livre d'édition des armors stands."));
		player.sendMessage(singleLineHelp("coords", "Permet d'envoyer rapidement vos coordonnées."));
		player.sendMessage(singleLineHelp("help", "Permet d'afficher l'aide aux commandes."));
		player.sendMessage(singleLineHelp("miniblocks", "Permet d'ouvrir le shop de mini bloc."));
		player.sendMessage(singleLineHelp("pack", "Permet d'ouvrir l'interface de customisation du BC Pack."));
		player.sendMessage(singleLineHelp("pay <PSEUDO> <MONTANT>", "Permet de transférer votre argent vers un joueur."));
	}
	
	private String singleLineHelp(String alias, String msg) {
		return ChatColor.GRAY + "- " + ChatColor.GOLD + "/bc " + alias + " " + ChatColor.DARK_GRAY + "➤ " + ChatColor.RESET + msg;
	}
	
	@SuppressWarnings("deprecation")
	private void pay(Player player, String[] args) {
		if(!Main.bddOn) {
			player.sendMessage(Other.bc + ChatColor.RED + "La connexion à la BDD au démarrage du serveur a échoué.");
			return;
		}
		
		if(args.length!=3) {
			player.sendMessage(Other.bc + ChatColor.RED + "Syntaxe : /bc pay <PSEUDO> <MONTANT>");
		}else {
			if(Main.getEconomy()!=null) {
				if(Main.getEconomy().hasAccount(player) && Main.getEconomy().hasAccount(args[1])) {
					double money;
					try {
						money = Double.parseDouble(args[2]);
					}catch(Exception e){
						 money = 0;
					}
					if(money>0 && Main.getEconomy().getBalance(player)>=money) {
						Main.getEconomy().withdrawPlayer(player, money);
						Main.getEconomy().depositPlayer(args[1], money);
						Other.sound_accepter(player);
						player.sendMessage(Other.bc + ChatColor.GREEN + "Vous venez d'envoyer " + ChatColor.YELLOW + money + " € " + ChatColor.GREEN + "à "  + args[1]);
						if(Bukkit.getPlayer(args[1]) != null && Bukkit.getPlayer(args[1]).isOnline()) 
							Bukkit.getPlayer(args[1]).sendMessage(Other.bc + ChatColor.GREEN + "Vous venez de recevoir " + ChatColor.YELLOW + money + " € " + ChatColor.GREEN + "de " + player.getName());
						return;
					}else {
						player.sendMessage(Other.bc + ChatColor.RED + "Le montant que vous souhaitez transférer n'est pas valide.");
					}
				}else {
					player.sendMessage(Other.bc + ChatColor.RED + "Votre compte ou celui du bénéficiaire n'existe pas.");
				}
			}else {
				player.sendMessage(Other.bc + ChatColor.RED + "Erreur : Impossible de récupérer l'économie du serveur.");
			}
		}
		Other.sound_refuser(player);
	}

	private void book(Player player) {
		player.sendMessage(Other.bc + ChatColor.GREEN + "Vous avez reçu votre livre pour éditer les armors stands.");
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "execute as " + player.getDisplayName() + " run function armor_statues:give");
	}
	
	private void coords(Player player) {
		Location pLoc = player.getLocation();
		Bukkit.broadcastMessage(ChatListener.heure() + ChatColor.GRAY + player.getDisplayName() + ChatColor.DARK_GRAY + " ➤ " + ChatColor.GOLD + "x: " + ChatColor.AQUA + pLoc.getBlockX()
				+ ChatColor.GRAY + " / " + ChatColor.GOLD + "y: " + ChatColor.AQUA + pLoc.getBlockY() + ChatColor.GRAY + " / " + ChatColor.GOLD + "z: " + ChatColor.AQUA + pLoc.getBlockZ());
	}
}