package me.jesuismister;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.jesuismister.afk.AFKTask;
import me.jesuismister.commands.BestCubeAdminTab;
import me.jesuismister.commands.BestCubeTab;
import me.jesuismister.commands.CommandsBC;
import me.jesuismister.commands.CommandsBCAdmin;
import me.jesuismister.crafts.CraftsCustoms;
import me.jesuismister.eventsListener.AnvilListener;
import me.jesuismister.eventsListener.ChatListener;
import me.jesuismister.eventsListener.DeathListener;
import me.jesuismister.eventsListener.HatsListener;
import me.jesuismister.eventsListener.MiniBlockShopListener;
import me.jesuismister.eventsListener.OtherListener;
import me.jesuismister.eventsListener.PreviewShopListener;
import me.jesuismister.eventsListener.ShopListener;
import me.jesuismister.eventsListener.SittingSlabListener;
import me.jesuismister.inventories.BCPack;
import me.jesuismister.inventories.MiniBlock;
import me.jesuismister.inventories.PreviewShop;
import me.jesuismister.inventories.Shop;
import me.jesuismister.sql.DS;
import me.jesuismister.tabMenu.TablistManager;
import me.jesuismister.tabMenu.TablistTask;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {
	private TablistManager tablistManager = new TablistManager();
	
    private static Economy econ = null;
    public static boolean bddOn = false;
    
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";

    @Override
    public void onEnable() {
        Bukkit.setWhitelist(true);
        DeathListener.main = this;

        saveDefaultConfig();
        PluginManager pm = Bukkit.getPluginManager();

        //On demarre tout ce qui est en rapport avec la BDD et l'�conomie
        if(setupEconomy() && DS.setupDS(this)) {
        	bddOn = true;
            initBDDRelative(pm);
            initTask();
            
            getCommand("bc").setExecutor(new CommandsBC());
    		getCommand("bc").setTabCompleter(new BestCubeTab());
    		getCommand("bcadmin").setExecutor(new CommandsBCAdmin());
    		getCommand("bcadmin").setTabCompleter(new BestCubeAdminTab());
            
            System.out.println(ANSI_GREEN + "Fin de la mise en place du plugin" + ANSI_RESET);
        }else {
        	System.out.println(ANSI_RED + "Annulation de la mise en place du plugin" + ANSI_RESET);
        }
        
        //On demarre le reste
        new CraftsCustoms(this);
        pm.registerEvents(new DeathListener(), this);
        pm.registerEvents(new AnvilListener(), this);
        pm.registerEvents(new SittingSlabListener(), this);
        pm.registerEvents(new ChatListener(this), this);
        pm.registerEvents(new HatsListener(), this);

        System.out.println();
        System.out.println(ANSI_GREEN + " __    __                       " + ANSI_RESET);
        System.out.println(ANSI_GREEN + "|__)  |     Le Plugin Best Cube " + ANSI_RESET);
        System.out.println(ANSI_GREEN + "|__)  |__   a fini de demarre   " + ANSI_RESET);
        System.out.println();
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    @Override
    public void onDisable() {
        System.out.println();
        System.out.println(ANSI_GREEN + " __    __                       " + ANSI_RESET);
        System.out.println(ANSI_GREEN + "|__)  |     Le Plugin Best Cube " + ANSI_RESET);
        System.out.println(ANSI_GREEN + "|__)  |__   s'est bien arrete   " + ANSI_RESET);
        System.out.println();
    }

    private void initBDDRelative(PluginManager pm) {
    	pm.registerEvents(new ShopListener(), this);
    	pm.registerEvents(new PreviewShopListener(), this);
        pm.registerEvents(new OtherListener(), this);
        pm.registerEvents(new MiniBlockShopListener(), this);
        
        Shop.createMapShops();
        Shop.setItemFromBDD();
        MiniBlock.createMiniBlockInv(this);
        PreviewShop.initPreviewShop();
        
        new BCPack(this);
    }
    
    private void initTask() {
    	TablistTask tablistTask = new TablistTask(this, econ);
		tablistTask.runTaskTimer(this, 0, 100L); //20x5 = 100 (5sec)
		
		AFKTask afkTask = new AFKTask();
		afkTask.runTaskTimer(this, 0, 20L); //20x1 = 20 (1sec)
	}

    public static Economy getEconomy() {
        return econ;
    }
    
    public TablistManager getTablistManager() {
		return tablistManager;
	}
}