package me.jesuismister.afk;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import me.jesuismister.Main;
import me.jesuismister.sql.Requetes;
import net.md_5.bungee.api.ChatColor;

public class AFKTask extends BukkitRunnable {
	Map<UUID, AFK> playerLocation = new HashMap<UUID, AFK>();
	
	@Override
	public void run() {
		String request = "UPDATE Player SET tpsJeu = tpsJeu + 1 WHERE ";
		int i = 0;
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			if(!playerLocation.containsKey(p.getUniqueId())) {
				playerLocation.put(p.getUniqueId(), new AFK(p.getLocation(), 0));
				request += addPlayerInMap(p.getUniqueId(), i);
				i++;
			}else {
				playerLocation.get(p.getUniqueId()).updateTime(p.getLocation());
				if(playerLocation.get(p.getUniqueId()).getTimeWithoutMoving()<180) {
					p.setPlayerListName(ChatColor.RESET + p.getName());
					p.setDisplayName(p.getName());
					request += addPlayerInMap(p.getUniqueId(), i);
					i++;
				}else {
					p.setPlayerListName(ChatColor.DARK_GRAY + "[AFK] " + ChatColor.GRAY + p.getName());
				}
			}
		}
		
		if(i>0 && Main.bddOn) Requetes.updateTimePlay(request);
	}
	
	private String addPlayerInMap(UUID uuid, int i) {
		if(i==0) return " uuid = '" + uuid.toString() + "'";
		else return " OR uuid = '" + uuid.toString() + "'";
	}

}
