package me.jesuismister.afk;

import org.bukkit.Location;

public class AFK {
	private int x;
	private int y;
	private int z;
	private int timeWithoutMoving;
	
	public AFK(Location loc, int timeWithoutMoving) {
		this.x = loc.getBlockX();
		this.y = loc.getBlockY();
		this.z = loc.getBlockZ();
		this.setTimeWithoutMoving(timeWithoutMoving);
	}

	public void updateTime(Location newLoc) {
		if(this.x==newLoc.getBlockX() && this.y==newLoc.getBlockY() && this.z==newLoc.getBlockZ()) {
			if(this.timeWithoutMoving<180) this.timeWithoutMoving += 1;
		}else{
			this.timeWithoutMoving = 0;
			this.x = newLoc.getBlockX();
			this.y = newLoc.getBlockY();
			this.z = newLoc.getBlockZ();
		}
	}

	public int getTimeWithoutMoving() {
		return timeWithoutMoving;
	}

	public void setTimeWithoutMoving(int timeWithoutMoving) {
		this.timeWithoutMoving = timeWithoutMoving;
	}

}
