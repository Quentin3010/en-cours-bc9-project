## Instalation de Java 17

J'ai demandé à TchatGPT de m'aider pour mettre en place proprement Java17 qui est nécessaire depuis la 1.19 de minecraft.

### Etape 1

Téléchargez le fichier tar.gz de la dernière version stable de Java à partir du site web d'Oracle (https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html).
```
wget https://download.oracle.com/java/17/archive/jdk-17.0.6_linux-x64_bin.tar.gz
```

Décompressez le fichier tar.gz en utilisant la commande suivante :
```
tar -xvf jdk-17.tar.gz
```

### Etape 2

Installez Java en copiant les fichiers décompressés dans un répertoire de votre choix, comme /opt/jdk-17 par exemple :
```
sudo mkdir /opt/jdk-17
```
```
sudo cp -r jdk-17/* /opt/jdk-17/
```

### Etape 3

Configurez votre système pour utiliser la nouvelle installation de Java en modifiant les variables d'environnement JAVA_HOME et PATH. Pour ce faire, vous pouvez éditer le fichier de configuration de l'utilisateur .bashrc :
```
export JAVA_HOME=/opt/jdk-17
```
```
export PATH=$JAVA_HOME/bin:$PATH
```

Rechargez les variables d'environnement en exécutant la commande suivante :
```
source ~/.bashrc
```


## Lancement du serveur en 1.19.3

### Etape 1

Créez un script de lancement pour votre serveur en utilisant un éditeur de texte tel que nano:
```
nano start.sh
```

Copiez le contenu suivant dans le fichier start.sh (spigot.jar <= spigot en 1.19.3):
```
#!/bin/bash
java -Xms1G -Xmx1G -jar spigot.jar
```

Rendez le fichier exécutable en utilisant la commande suivante:
```
chmod +x start.sh
```

### Etape 2

Lorsque vous exécutez un serveur Minecraft sur votre VPS, celui-ci écoutera les connexions entrantes sur un certain port, défini dans le fichier de configuration server.properties. Pour que les joueurs puissent se connecter à votre serveur, il est nécessaire de configurer votre pare-feu pour autoriser les connexions entrantes sur ce port.

Si votre VPS utilise ufw (Firewall Uncomplicated), vous pouvez autoriser les connexions entrantes sur le port défini en utilisant la commande suivante:
```
sudo ufw allow <port>/tcp
```
où <port> est le numéro de port défini dans votre fichier de configuration server.properties.

### Etape 3

On ouvre une fenêtre sur le VPN pour faire tourner le serveur en continue :
```
screen -S minecraft
```

Et on lance le serveur
```
./start.sh
```

Normalement, si tout ce passe bien, le serveur MC va démarrer. On peut désormais fermer la fenêtre, et aller en jeu pour se connecter sur le serveur à l'adresse suivante : IP:PORT

Si je veux réouvrir plus tard la console, je peux faire la commande suivante pour réafficher l'écran :
```
screen -r minecraft
```