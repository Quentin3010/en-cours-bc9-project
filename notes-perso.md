# Projet BC9

## Mise en place de la BDD

Pour la mise en place de la BDD mysql, j'ai suivis le tutoriel suivant : https://youtu.be/a4TZ9y39eKY
Ce qui va suivre, c'est les notes que j'ai pris.

### Etapes 1

Connection au VPS en ssh
```
ssh USER@IP -p PORT
```

Passage en admin :
```
sudo su -
```

### Etapes 2

Téléchargement de apache et autres :
```
sudo apt -y install wget php php-cgi php-mysqli php-pear php-mbstring libapache2-mod-php php-common php-phpseclib php-mysql apache2 unzip
```

Aller sur https://www.phpmyadmin.net/ pour récupérer la dernière version (ici 5.2.0):
```
wget https://files.phpmyadmin.net/phpMyAdmin/5.2.0/phpMyAdmin-5.2.0-all-languages.zip
```

### Etapes 3

On bouge et extrait le contenu du zip dans /var/www :
```
mv phpMyAdmin-5.2.0-all-languages.zip /var/www/
```
```
cd /var/www
```
```
apt install unzip
```
```
unzip phpMyAdmin-5.2.0-all-languages.zip
```

### Etapes 4

Se rendre dans le fichier "000-default.conf" pour modifier la variable "DocumentRoot"
```
cd /etc/apache2/sites-enabled/
```
```
nano 000-default.conf
```
-> "DocumentRoot"
	-> avant : /var/www/html
	-> après : /var/www/phpMyAdmin-5.2.0-all-languages
	
```
service apache2 restart
```

### Etape 5

On édite la ligne "bind-adress" où on remplace "127.0.0.1" par "0.0.0.0"
```
/etc/mysql/mariadb.conf.d/50-server.cnf
```

### Etapes 6

On installe mariadb-server :
```
apt install mariadb-server
```

### Etapes 7

On se connecte à la base et on créé un utilisateur
```
mysql -u root
```
```
create user 'qbernard'@'localhost' identified by 'MOT_DE_PASSE';
```
```
GRANT ALL PRIVILEGES ON nom_base_de_donnees.* TO 'qbernard'@'%' IDENTIFIED BY 'MOT_DE_PASSE';
```
```
FLUSH PRIVILEGES;
```

### On check que tout marche

Ouvrir un navigateur web, et se rendre sur "IP_DU_VPS".
Rentrer le nom d'utilisateur et le mot de passe de l'utilisateur qu'on a créer précédemment.
On est connecté !!

### Fix problème n°1

Quand on se connecte sur mysql, on voit que tout en bas de la page, il y a des erreurs dans un encadré rouge, pour fixer la première, il faut affecter une chaine de caractère de 32 octets au champ 'blowfish_secret' dans le fichier /var/www/phpMyAdmin-5.2.0-all-languages/config.sample.inc.php

```
nano /var/www/phpMyAdmin-5.2.0-all-languages/config.sample.inc.php
```
-> On remplis avec 32 caractères le champ de 'blowfish_secret' :
	-> $cfg['blowfish_secret'] = 'yEVM0ayOqWlGFQVxBseJvJfzxzEmca4x'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */

### Fix problème n°2

Pour fixe le 2ème problème, il suffit de créer un dossier "/var/www/phpMyAdmin-5.2.0-all-languages/tmp/" et de donner les permissions de celui-ci :
```
mkdir /var/www/phpMyAdmin-5.2.0-all-languages/tmp/
```
```
chmod -R 777 /var/www/phpMyAdmin-5.2.0-all-languages/tmp/
```

## Créer les tables + insérer les valeurs

```
mysql -u qbernard -p < sql_create_tables.sql
```
```
mysql -u qbernard -p < sql_requests.sql
```