# BC9 Plugin

Quentin BERNARD

## Description du projet

Le projet "BC9 Plugin" est un plugin Java pour Minecraft qui vise à améliorer l'expérience de jeu en introduisant des fonctionnalités supplémentaires. 

Ce plugin offre la possibilité aux joueurs d'accéder à un shop intégré, où ils peuvent acheter diverses ressources et objets. Cette fonctionnalité permet de mettre en place une économie virtuelle dans le jeu, où les joueurs peuvent effectuer des transactions d'argent entre eux. 

En plus du système de shop, le plugin offre également des fonctionnalités de personnalisation, comme la possibilité d'ajouter des chapeaux personnalisés pour les personnages du joueur. Ces ajouts visent à rendre le jeu plus amusant et offrent aux joueurs une expérience unique et enrichissante dans l'univers de Minecraft.

Vidéo de présentation : lien

## Fonctionnalités

https://docs.google.com/document/d/e/2PACX-1vRog-WfvoEJlmYR_X9-7FGz5266iT4xmJBZ1R2rI0G-Js_XDnaC9pXhDkY-O6brhbSbhjSuPeE7aeXl/pub

Pour ce projet, j’ai décidé d’héberger la base de donnée sur mon propre VPS, ce qui m’a donc demandé de le mettre en place + mettre en place mysql dessus.

Veuillez noter que certaines fonctionnalités nécessitent des éléments supplémentaires tels qu'un datapack, une base de données ou un resource pack pour un rendu complet. Le plugin a été conçu pour être utilisé principalement par l'auteur et ses amis, mais le code source a été rendu public pour que d'autres puissent l'explorer et tester la plupart des fonctionnalités proposées. Ainsi, aucun tutoriel d’installation ne sera fourni.

